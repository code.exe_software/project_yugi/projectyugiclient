/**
 * @author Daniel Murker
 * 
 * Main splash screen of the game.  Entry point.  start the music!
 */

package States { 
	import flash.display.MovieClip;
	import flash.text.TextFieldType;
	import flash.utils.getDefinitionByName;
	
	import Core.DisplayManager;
	import Core.InputManager;
	import Core.SoundManager;
	
	import Events.BaseEvent
	import Events.NetworkEvent;
	import Events.StateMachineEvent;
	
	import Interfaces.StateInterface;
	
	import NetworkCommands.MainSplashStateCommands;
	
	import Utils.Cache;
	import Utils.EventBus;
	import Utils.Promise.Deferred;
	
	public class SettingsState implements StateInterface {
		protected var m_asset:MovieClip;
		
		public function init():Deferred {
			
			var cachePromise:Deferred = Cache.load('/assets/swf/settings.swf');
			cachePromise.then(continueInit);
			
			return cachePromise;
		}
		
		protected function continueInit():void {
			m_asset = new (getDefinitionByName('assets.Settings'))();
			m_asset.x = 0;
			m_asset.y = 0;
			
			m_asset.menuAudio.visible = false;
			m_asset.menuKeys.visible = false;
			m_asset.menuVideo.visible = true;
		}
		
		public function enter(data:Object = null):void {
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btnAudio, showAudioSettings);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btnVideo, showVideoSettings);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btnKeys, showKeyBindings);
			
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.menuAudio.btnSfxMute, sfxMute);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.menuAudio.btnSfxPlay, sfxPlay);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.menuAudio.btnMusicMute, musicMute);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.menuAudio.btnMusicPlay, musicPlay);
			
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.menuVideo.btnFullscreen, videoFullscreen);
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.menuVideo.btnNormal, videoNormal);
			
			InputManager.getInstance().registerMouse(InputManager.MOUSE_CLICK, m_asset.btnBack, backToMain);
			
			
			DisplayManager.addToDisplay(m_asset, DisplayManager.GAMELAYER);
			
			//SoundManager.getInstance().playMusic(SoundManager.MUSIC_MAIN);
		}
		
		public function exit():void {
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btnAudio, showAudioSettings);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btnVideo, showVideoSettings);
			InputManager.getInstance().unregisterMouse(InputManager.MOUSE_CLICK, m_asset.btnKeys, showKeyBindings);
			
			DisplayManager.removeFromDisplay(m_asset, DisplayManager.GAMELAYER);
		}
		
		
		public function timerUpdate(delta:int):void { }
		
		protected function enterGame(target:Object):void {
			SoundManager.getInstance().playSfx('click');
			
			var commandObj:Object = MainSplashStateCommands.getInstance().enterGame();
			var networkEvent:NetworkEvent = new NetworkEvent(NetworkEvent.SOCKET_DATA_WRITE);
			EventBus.dispatch(networkEvent, commandObj);
			//SoundManager.getInstance().stopMusic();
		}
		
		protected function showAudioSettings(target:Object):void {
			m_asset.menuAudio.visible = true;
			m_asset.menuKeys.visible = false;
			m_asset.menuVideo.visible = false;
		}
		
		protected function showVideoSettings(target:Object):void {
			m_asset.menuAudio.visible = false;
			m_asset.menuKeys.visible = false;
			m_asset.menuVideo.visible = true;
		}
		
		protected function showKeyBindings(target:Object):void {
			m_asset.menuAudio.visible = false;
			m_asset.menuKeys.visible = true;
			m_asset.menuVideo.visible = false;
		}
		
		protected function sfxMute(target:Object):void {
			SoundManager.getInstance().disableSfx();
		}
		
		protected function sfxPlay(target:Object):void{
			SoundManager.getInstance().enableSfx();
			SoundManager.getInstance().playSfx('click');
		}
		
		protected function musicMute(target:Object):void{
			SoundManager.getInstance().disableMusic();
			SoundManager.getInstance().playSfx('click');
		}
		
		protected function musicPlay(target:Object):void{
			SoundManager.getInstance().enableMusic();
			SoundManager.getInstance().playSfx('click');
		}
		
		protected function videoFullscreen(target:Object):void{
			SoundManager.getInstance().playSfx('click');
			EventBus.dispatch(new BaseEvent('FULLSCREEN')) 
		}
		
		protected function videoNormal(target:Object):void{
			SoundManager.getInstance().playSfx('click')
			EventBus.dispatch(new BaseEvent('NORMAL'))
		}
		
		protected function backToMain(target:Object):void{
			SoundManager.getInstance().playSfx('click');
			EventBus.dispatch(new StateMachineEvent(StateMachineEvent.STATEMACHINE_CHANGESTATE), {
				statename: "MainMenu"
			});
		}
	}
}