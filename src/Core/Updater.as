package Core 
{
	import Interfaces.UpdaterInterface;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	/**
	 * ...
	 * @author ...
	 */
	public class Updater 
	{
		private static var s_instance:Updater;
		private const TIMEINTERVAL:int = 50;
		
		private var m_paused:int;
		
		private var m_loops:Array;
		private var m_unpausableLoops:Array;

		private var m_interval:Timer;
		
		
		public function Updater(key:SingletonEnforcer = null) {
			if (!key) {
				throw new Error("BAD MONKEY!  NO UPDATER FOR YOU");
			}
			
			m_loops = [];
			m_unpausableLoops = [];
			m_paused = 0;
		}
		
		public static function getInstance():Updater {
			if (!s_instance) {
				s_instance = new Updater(new SingletonEnforcer());
			}
			
			return s_instance;
		}
		
		public function init():void {
			m_interval = new Timer(TIMEINTERVAL);
			m_interval.addEventListener(TimerEvent.TIMER, update);
			m_interval.start();
		}
		
		public function addListener(listener:UpdaterInterface, pausable:Boolean = true):void {
			m_loops.push(listener)
			if (!pausable) {
				m_unpausableLoops.push(listener);
			}
		}
		
		public function removeListener(listener:UpdaterInterface):void {
			var loopIndex:int = m_loops.indexOf(listener);
			var unpausableLoopIndex:int = m_unpausableLoops.indexOf(listener);
			if (~loopIndex) {
				m_loops.splice(loopIndex, 1);
			}
			if (~unpausableLoopIndex) {
				m_unpausableLoops.splice(unpausableLoopIndex, 1);
			}
			
		}
		
		private function update(e:TimerEvent):void {
			if (m_paused > 0) {
				updateUnpausable();
			}
			else {
				updateAll();
			}
		}
		
		private function updateAll():void {
			for (var i:* in m_loops) {
				m_loops[i].timerUpdate(TIMEINTERVAL);
			}
		}
		
		private function updateUnpausable():void {
			for (var i:* in m_unpausableLoops) {
				m_unpausableLoops[i].timerUpdate(TIMEINTERVAL);
			}
		}
		
	}

}

class SingletonEnforcer{}