package Core 
{	
	/**
	 * @todo always verify that the network.json address for the security server is an IP address.  otherwise flash chokes. --DM
	 */
	import flash.utils.getDefinitionByName;
	import Events.NetworkEvent;
	
	import Utils.EventBus;
	import Events.*;
	
	import JSON;
	
	//import com.adobe.serialization.json.JSON;
	
	public class NetworkManager
	{
		private static var s_instance:NetworkManager;
		private var m_sockets:Object = {}; //named list of socketConnections
		
		//private var m_connected:Boolean = false;
		
		public function NetworkManager(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO NETWORK MANAGER");
			}
		}
		
		public static function getInstance():NetworkManager {
			if (!s_instance) {
				s_instance = new NetworkManager(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function init():void {
			var networkJson:Object = EmbeddedData.getData(EmbeddedData.NETWORK);
			var socketServerJson:Object = networkJson.SocketServer;
			m_sockets['MAIN'] = new SocketConnection('MAIN');
			m_sockets['MAIN'].connect(socketServerJson.host, int(socketServerJson.port));
			
			EventBus.listen(NetworkEvent.SOCKET_DATA_WRITE, handleWrite);
			EventBus.listen(NetworkEvent.CREATE_NEW_CONNECTION, createNewConnection);
		}
		
		
		public function makeNewConnection(name:String, host:String, port:int):void {
			m_sockets[name] = new SocketConnection(name);
			m_sockets[name].connect(host, port);
		}
		
		protected function handleWrite(e:NetworkEvent):void { 
			var commandData:Object = e.getData();
			var socketName:String;
			if (commandData.hasOwnProperty('socketName')) {
				socketName = commandData.socketName;
				delete commandData.socketName;
			} else {
				socketName = 'MAIN';
			}
			var command:String = JSON.stringify(commandData);
			
			m_sockets[socketName].write(command);
		}
		
		/**
		 * @todo gotta come up with a name for this connection that's not just the port number, heh.
		 * @param	event
		 */
		protected function createNewConnection(event:BaseEvent):void {
			var eventData:Object = event.getData();
			var location:String = eventData.loc;
			var locComponents:Array = location.split(':');
			var connectionName:String = eventData.name || 'GAME';
			var host:String = locComponents[0]
			var port:int = parseInt(locComponents[1]);
			
			
			makeNewConnection(connectionName, host, port);
		}
	}
}

//force network manager to be a singleton.
class SingletonEnforcer { }

import flash.net.Socket;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.events.ProgressEvent;
import Utils.EventBus;
import Events.*;

import JSON;


class SocketConnection {
	protected var m_connected:Boolean = false;
	protected var m_socket:Socket = new Socket();
	protected var m_socketName:String;
	protected var m_unfinishedCommand:String = '';
	
	public function SocketConnection(name:String) {
		m_socketName = name;
		
		m_socket.addEventListener(Event.CONNECT, handleConnect);
		m_socket.addEventListener(ProgressEvent.SOCKET_DATA, handleSocketData);
		m_socket.addEventListener(Event.CLOSE, handleClose);
		m_socket.addEventListener(IOErrorEvent.IO_ERROR, handleIoError);
		m_socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleSecurityError); 
	}
	
	public function connect(host:String, port:int):void {
		m_socket.connect(host, port);
	}
	
	public function write(command:String):void {
		if (m_connected) {
			m_socket.writeUTFBytes(command);
			m_socket.writeUTFBytes('##');
			m_socket.flush();
		}else {
			EventBus.require(NetworkEvent.SOCKET_CONNECTED + '_' + m_socketName, function():void {
				write(command);
			});
		}	
	}
	
	protected function handleConnect(e:Event):void {
		m_connected = true;
		EventBus.dispatch(new NetworkEvent(NetworkEvent.SOCKET_CONNECTED + '_' + m_socketName ), { } );
	}
	
	protected function handleSocketData(e:ProgressEvent):void {
		while (m_socket.bytesAvailable) {
			var str:String = m_socket.readUTFBytes(m_socket.bytesAvailable);
			var commands:Array = str.split('##');
			
			for (var i:String in commands) {
				if (!commands[i]) { //usually an empty string in this case.
					continue;
				}
					
				var command:Object;
				
				if (m_unfinishedCommand) {
					m_unfinishedCommand += commands[i];
					commands[i] = m_unfinishedCommand;
					m_unfinishedCommand = "";
				} 
				
				try {
					command = JSON.parse(commands[i]);
					EventBus.dispatch(new BaseEvent(command.command), command.data);
				} catch (e:Error) {
					m_unfinishedCommand = commands[i];
				}
			}
		}
	}
	
	/**
	 * connection close stub
	 * @param	e
	 */
	protected function handleClose(e:Event):void {
trace("SOCKET " + m_socketName + " CLOSED");
		m_socket.close();
	}
	
	/**
	 * io error event handler stub
	 * @param	e
	 */
	protected function handleIoError(e:IOErrorEvent):void {	}
	
	/**
	 * security event handler stub
	 * @param	e
	 */
	protected function handleSecurityError(e:SecurityErrorEvent):void {	}
}