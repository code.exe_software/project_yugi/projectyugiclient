package NetworkCommands
{
	import Events.MainMenuEvent;
	
	public class MainMenuStateCommands
	{
		protected static var s_instance:MainMenuStateCommands;
		
		public function MainMenuStateCommands(singletonEnforcer:SingletonEnforcer = null) {
			if (!singletonEnforcer) {
				throw new Error ("BAD MONKEY!  NO MainMenuStateCommands!");
			}
		}
		
		public static function getInstance():MainMenuStateCommands {
			if (!s_instance) {
				s_instance = new MainMenuStateCommands(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function enterModeSelect():Object {
			var result:Object = {
				event: MainMenuEvent.MODE_SELECT,
				data: {}
			};
			
			return result;
		}
		
		public function enterSettings():Object {
			var result:Object = {
				event: MainMenuEvent.SETTINGS,
				data: {}
			};
			
			return result;
		}
		
		/*public function seekPlayer():Object {
			var result:Object = {
				event: GameStateEvent.SEEK_PLAYER,
				data: {//@todo player stats should go in here for ranking matchups maybe?}
			};
			
			return result;
		}*/
	}
}

class SingletonEnforcer {}