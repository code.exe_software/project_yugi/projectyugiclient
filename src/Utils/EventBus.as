/**
 * A centralized point for events
 * This used to have listeners seperated by context, but that would require every class in the game being an event dispatcher, which makes no sense in as3.
 * the javascript version of that works, since all objects can be used as contexts for callbacks.  stupid flash.  --DM
 * 
 * REQUIRE uses the promise system such that I can use it to prevent race conditions involving events that might already have fired.
 * although that might be overkill since it was only introduced to try debugging an issue with listenonce which turned out 
 * to be the fact that I was using 'useWeakReference = true' like a dumbass.  Oh well. 
 * For now it's a nice shortcut so I don't have to check values like 'isinitialized' to check to see if I should fire a function
 * or listen to an event.
 */ 

package Utils
{

	import Events.BaseEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import Utils.Promise.Deferred;
		
	public class EventBus
	{
		protected static var s_activeDispatcher:EventDispatcher;
		protected static var s_promises:Object = {};
		
		//////////////////////
		//	Public Functions
		//////////////////////
		public static function init(dispatcher:EventDispatcher):void {
			if (!s_activeDispatcher) {
				s_activeDispatcher = dispatcher;
			}
		}
		
		/**
		 * Wraps IEventDispatcher.addEventListener so that it can add false, 0, true as the last 3 params of the method call, thus slightly helping potential memory issues
		 */
		public static function listen(event:String, listener:Function, useCapture:Boolean = false, priority:int=0, useWeakReference:Boolean = false):void
		{
			//initialize the dispatchers array if it hasn't been used yet.
			s_activeDispatcher = s_activeDispatcher || new EventDispatcher;
			
			//var index:int = _getDispatcherIndex(dispatcher);
			
			//add the listener to the dispatcher.  
			//also, this preserves the old event bus method of forcing events to nt use capture, a 0 priority, and useWeakReference = true
			s_activeDispatcher.addEventListener(event, listener, useCapture, priority, useWeakReference);
			
		}
		
		/**
		 * Wrap addEventListener in such a way as to automatically remove the listener after the first subsequent event fire.
		 */
		public static function listenOnce(event:String, listener:Function, useCapture:Boolean = false, priority:int=0, useWeakReference:Boolean = false):void
		{
			//initialize the dispatchers array if it hasn't been used yet.
			s_activeDispatcher = s_activeDispatcher || new EventDispatcher;
			
			s_activeDispatcher.addEventListener(event, function(e:Event):void {
				//remembered this from my javascript days, regarding removing anon listeners from event dispatchers.
				//Sexy, no?
				s_activeDispatcher.removeEventListener(event, arguments.callee);
				listener(e);
			},useCapture, priority, useWeakReference);
		}
		
		/**
		 * Wraps IEventDispatcher.removeEventListener so that we could potentially do cool stuff here =D
		 */
		public static function stopListening(event:String, listener:Function):void
		{
			s_activeDispatcher.removeEventListener(event, listener);
		}
		
		/**
		 * fires off an event to a specified dispatcher, or to all dispatchers if no dispatcher is specified.
		 */
		public static function dispatch( event:BaseEvent, data:Object = null):void
		{
			if (data) {
				event.setData(data);
			}
			s_activeDispatcher.dispatchEvent(event);		
		}
		
		public static function willTrigger(dispatcher:EventDispatcher, type:String):Boolean {
			return s_activeDispatcher.willTrigger(type)
		}
		
		public static function require(event:String, listener:Function):void {
			if (!s_promises.hasOwnProperty(event)) {
				var deferred:Deferred = new Deferred;
				
				listenOnce(event, function(e:Event):void {
					deferred.resolve();
				});
				
				s_promises[event] = deferred;
			}
			s_promises[event].then(listener);	
		}
	}
}