/**
 * this defines a pair of blocks that can be affected by input, and controls the random generation of the blocks that appear.
 * @TODO modify gameblock in a similar manner to gamechunk, such that an event is fired when each block is initialized allowing the chunk to populate itself
 */

package GameObjects 
{
	import Events.GameStateEvent;
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	
	import GameObjects.gameBlock;
	import Utils.Cache;
	import Utils.EventBus;
	import Core.InputManager;
	import Events.GameStateEvent;
	
	public class gameChunk
	{
		private var m_asset:MovieClip;
		private var m_data:Object;
		private var m_block1:gameBlock;
		private var m_block2:gameBlock;
		private var m_initializedBlocksCount:int = 0;
		
		private var m_position:int = 0;
		
		private var m_bounding:Object = { left: false, right: false, lower: false };
		
		
		/**
		 * add gameblocks to the chunk at the positions.
		 * set up event listeners 
		 * 
		 * @param data object {block1: "color", block2: "color"}
		 */
		public function gameChunk(data:Object) 
		{
			m_data = data;
			Cache.load('/assets/swf/gameboard.swf', init); 
		}
		
		public function init():void {
			m_asset = new (getDefinitionByName('assets.gameChunk'))();
			m_asset.stop();
			
			EventBus.listen(GameStateEvent.GAME_BLOCK_INITIALIZED, addBlocks);
			m_block1 = new gameBlock(m_data.block1);
			m_block2 = new gameBlock(m_data.block2);
			
		}
		
		public function isInitialized():Boolean {
			return this.m_initializedBlocksCount == 2;
		}
		
		private function addBlocks(e:GameStateEvent):void {
			if (e.getData() == m_block1) {
				m_asset.position1.addChild(m_block1.getAsset());
				++m_initializedBlocksCount;
			}
			
			if (e.getData() == m_block2) {
				m_asset.position2.addChild(m_block2.getAsset());
				++m_initializedBlocksCount;
			}
			
			if (m_initializedBlocksCount < 2)
			{
				return;
			}
			
			var initializedEvent:GameStateEvent = new GameStateEvent(GameStateEvent.GAME_CHUNK_INITIALIZED);
			initializedEvent.setData(this);
			EventBus.dispatch(initializedEvent);
			
		}
		
		public function getAsset():MovieClip {
			return m_asset;
		}
		
		protected function updatePosition():void {
			m_asset.gotoAndStop("position" + (m_position));
		}
		
		public function getPosition():int {
			return m_position;
		}
		
		public function setPosition(newPos:int):void {
			m_position = newPos;
			
			updatePosition();
		}
		
		public function rotateLeft(rotations:int = 1):void {
			m_position -= rotations;
			if (m_position < 0) {
				m_position += 4;
			}
			updatePosition();
		}
		
		public function rotateRight(rotations:int = 1):void {
			m_position += rotations;
			if (m_position > 3) {
				m_position -= 4;
			}
			updatePosition();
		}
		
		/**
		 * BOUNDING FUNCTIONS
		 */
		public function setLeftBounding(value:Boolean):void {
			m_bounding['left'] = value;
		}
		
		public function getLeftBounding():Boolean {
			return m_bounding['left'];
		}
		
		public function setRightBounding(value:Boolean):void {
			m_bounding['right'] = value;
		}
		
		public function getRightBounding():Boolean {
			return m_bounding['right'];
		}
		
		public function setLowerBounding(value:Boolean):void {
			m_bounding['lower'] = value;
		}
		
		public function getLowerBounding():Boolean {
			return m_bounding['lower'];
		}
		
		public function settle():Array {
			EventBus.stopListening(GameStateEvent.GAME_BLOCK_INITIALIZED, addBlocks);
			
			m_asset.position1.removeChild(m_block1.getAsset());
			m_asset.position2.removeChild(m_block2.getAsset());
			
			return [m_block1, m_block2];
		}
		
	}

}