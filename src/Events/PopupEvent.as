package Events 
{
	/**
	 * ...
	 * @author 
	 */
	public class PopupEvent extends BaseEvent
	{
		public static const POPUP_INITIALIZED:String = "events.popups.initialized";
		public static const POPUPS_CLOSE:String = "events.popups.close";
		
		public static const OTHER_PLAYER_FOUND:String = "otherPlayerFound";
		public static const BOTH_PLAYERS_READY:String = "bothPlayersReady";
		
		public function PopupEvent(event:String = null) 
		{
			super(event);
		}
		
	}

}