/**
 * ...
 * @author ...
 */
package Events 
{
	
	public class LobbyStateEvent extends BaseEvent 
	{
		
		public static const LOBBY_STATE_INITIALIZED:String = "states.lobbyState.initialized";
		
		public function LobbyStateEvent(event:String = null ) 
		{
			super(event);
		}
		
	}

}